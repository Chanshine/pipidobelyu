from django.shortcuts import render, redirect
from .forms import PerForm
from .models import Person

# Create your views here.

def index(request):
    if (request.method == "POST"):
        form = PerForm(request.POST)
        if form.is_valid():
            request.session['input'] = request.POST
            request.session['name'] = request.POST['name']
            request.session['status'] = request.POST['status']
            return redirect('verify')
    form = PerForm()
    data = Person.objects.all()
    return render(request, 'index.html', {'form' : form, 'data' : data})

def verify(request):
    if (request.method == "POST"):
        if 'pos' in request.POST:
            form = PerForm(request.session.get('input'))
            form.save()
            return redirect('/')
        elif 'neg' in request.POST:
            return redirect('/')
    name = request.session.get('name')
    status = request.session.get('status')
    return render(request, 'verify.html', {'name' : name, 'status' : status})