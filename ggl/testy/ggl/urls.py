from django.urls import path

from .views import index, verify

urlpatterns = [
    path('', index, name='index'),
    path('verify', verify, name='verify')
]