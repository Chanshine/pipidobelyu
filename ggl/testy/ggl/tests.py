import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from .views import index, verify
from .models import Person
from .forms import PerForm

# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_index(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_model_can_create_new(self):
        person = Person.objects.create()
        count_all = Person.objects.all().count()
        self.assertEqual(count_all, 1)

    def test_model_can_create_new_item(self):
        person = Person(name='lab ppw', status='lab 7')
        name = person.name
        status = person.status
        self.assertEqual(name,'lab ppw')
        self.assertEqual(status,'lab 7')
        
    def test_post_success(self):
        form_data = {'name' : 'lab ppw', 'status' : 'lab 7'}
        form = PerForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_post_error(self):
        form_data = {'name' : '', 'status' : ''}
        form = PerForm(data=form_data)
        self.assertFalse(form.is_valid())

class Lab7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab7UnitTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        name = selenium.find_element_by_id('id_name')
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        name.send_keys('Selenium : Hello World !')
        status.send_keys('TDD : Capek')
        submit.send_keys(Keys.RETURN)
