from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

# Create your views here.

def signUp(request):
    form = UserCreationForm()
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, "Successfully created a user with username {}".format(username))
            return redirect('loginUser')
    return render(request, 'signup.html', {'form' : form})

def loginUser(request): 
    if request.method == 'POST': 
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'Logged in as {}'.format(username))
            return redirect('pages')
        else:
            messages.error(request, 'Username or password is incorrect')
    return render(request, 'login.html')

def logoutUser(request):
    logout(request)
    messages.success(request, 'Successfully logged out!')
    return redirect('loginUser')

def pages(request):
    return render(request, 'pages.html')