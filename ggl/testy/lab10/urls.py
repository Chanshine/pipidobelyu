from django.urls import path
from .views import signUp, loginUser, logoutUser, pages
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('signup/', signUp, name="signUp"),
    path('login/', loginUser, name="loginUser"),
    path('logout/', logoutUser,name="logoutUser"),
    path('pages/', pages, name="pages")
]