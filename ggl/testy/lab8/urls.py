from django.urls import path
from .views import accordion

urlpatterns = [
    path('', accordion)
]