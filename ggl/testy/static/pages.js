$(function(){
var canvas = $('<canvas>');
var zoom = 4;
var WIDTH = 360*zoom;
var HEIGHT = 512*zoom;
var _WIDTH = WIDTH;
var _HEIGHT = HEIGHT;
var FPS = 60;
var TIME = 0;

$(canvas).attr({
    'width':WIDTH,
    'height':HEIGHT
});

var resizeCanvas = function(){
    var dim = {
        w:$(window).width(),
        h:$(window).height()
    };
    if(WIDTH/HEIGHT > dim.w/dim.h){
        _WIDTH = dim.w;
        _HEIGHT = dim.w*HEIGHT/WIDTH;
    }else{
        _HEIGHT = dim.h;
        _WIDTH = dim.h*WIDTH/HEIGHT;
    }
    $(canvas).css({
        'position':'absolute',
        'left':(dim.w-_WIDTH)/2,
        'top':(dim.h-_HEIGHT)/2,
        'width':_WIDTH,
        'height':_HEIGHT
    });

};

$('body').append(canvas);
var c = canvas[0].getContext('2d');
var mouse = {x:0,y:0,clicked:false};
$(canvas).click(function(e){
    var rect = canvas[0].getBoundingClientRect();
    coord = [e.clientX-rect.left,e.clientY-rect.top];
    mouse.x = Math.floor(coord[0]*WIDTH/_WIDTH);
    mouse.y = Math.floor(coord[1]*HEIGHT/_HEIGHT);
    mouse.clicked = true;
});

var randRange = function(x,y){
    return x+Math.random()*(y-x);
};
var getDist = function(x1,y1,x2,y2){
    return Math.sqrt(Math.pow(x1-x2,2)+Math.pow(y1-y2,2));
}

var sprite = function(url,xorig,yorig){
    var image = new Image();
    image.src = url;
    return {
        img:image,
        x:xorig,
        y:yorig
    };
};
var Entities = [];
var newEntity = function(x,y,spr){
    var self = {
        x:x,
        y:y,
        spr:spr,
        time:0,
        img_angle:0,
        img_alpha:1,
        img_xscale:1,
        img_yscale:1
    };
    self.destroy = function(){
        Entities.splice(Entities.indexOf(self),1);
    };
    self.move = function(){
        with(self){
            x++;
        }
    };
    self.draw = function(){
        with(self){
            img_alpha = Math.min(1,img_alpha);
            img_alpha = Math.max(0,img_alpha);
            c.save();
            var coords = map({x:x,y:y});
            c.translate(coords.x,coords.y);
            c.rotate(-img_angle*Math.PI/180);
            c.scale(img_xscale,img_yscale);
            c.globalAlpha = img_alpha;
            c.drawImage(spr.img,-spr.x,-spr.y);
            c.restore();
        }
    };
    self.update = function(){
        self.move();
        self.draw();
        self.time++;
    };
    Entities.push(self);
    return self;
};
var cam = {x:0,y:0};
var sprMap = sprite('https://i.postimg.cc/02sJmLFK/light-lake.png',0,0);
var map = function(coords){
    return {
        x:coords.x+WIDTH/2-cam.x,
        y:coords.y+HEIGHT/2-cam.y
    };
};
sprMap.scale = 6;
var room = {
    w:3840,//sprMap.img.width*sprMap.scale,
    h:2880//sprMap.img.height*sprMap.scale,
};

var sprPlayers = sprite('https://i.postimg.cc/cCBLG7w1/FULL-SPRITE.png',0,0);
var drawPlayer = function(){
    with(this){
        img_alpha = Math.min(1,Math.max(0,img_alpha));
        c.globalAlpha = img_alpha;
        imgIndex %= 4;
        dir %= 4;
        while(imgIndex < 0){imgIndex += 4;}
        while(dir < 0){dir += 3;}
        var sep = {x:spr.img.width/15,y:spr.img.height/8};
        var index = {x:3*charIndex.x,y:4*charIndex.y};
        index.y += (dir === 2)+2*(dir === 0)+3*(dir === 1);
        index.x += Math.floor(imgIndex);
        if(Math.floor(imgIndex) == 3){
            index.x -= 2;
        }
        var rect = [index.x*sep.x,index.y*sep.y,sep.x,sep.y];
        var coordsXY = map({x:x-sep.x/2*img_xscale,y:y-sep.y*img_xscale+50});
        c.drawImage(spr.img,rect[0],rect[1],rect[2],rect[3],coordsXY.x,coordsXY.y,rect[2]*img_xscale,rect[3]*img_xscale);
    }
}
var canTalk = false;
var isTalking = false;
var curDialog = '';
var letterIndex = 0;
var chars = [];
var player = newEntity(room.w/2,room.h/2,sprPlayers);
player.dir = 0;
player.charIndex = {x:4,y:1};
player.imgIndex = 1;
player.draw = drawPlayer;
player.img_xscale = 12;
player.img_yscale = 12;
player.toX = player.x;
player.toY = player.y;
player.spd = 13;
player.isMoving = false;
player.move = function(){
    with(player){
        canTalk = false;
        if(isMoving){
            imgIndex += 0.1;
            x += spd*Math.cos(dir*Math.PI/2);
            y -= spd*Math.sin(dir*Math.PI/2);
        }else{
            imgIndex = 1;
            for(var i in chars){
                if(getDist(x,y,chars[i].x,chars[i].y) < 300){
                    canTalk = true;
                    chars[i].isMoving = false;
                    chars[i].timeInt = chars[i].time+1;
                    chars[i].toX = chars[i].x;
                    chars[i].toY = chars[i].y;
                }
            }
        }
        if(Math.abs(x-toX) < spd){
            x = toX;
            if(Math.abs(y-toY) < spd){
                y = toY;
                isMoving = false;
            }else{
                dir = 1+2*(y < toY);
            }
        }else{
            dir = 2*(x > toX);
        }
        cam.x = Math.max(WIDTH/2,Math.min(room.w-WIDTH/2,x));
        cam.y = Math.max(HEIGHT/2,Math.min(room.h-HEIGHT/2,y));
        if(mouse.clicked && !isTalking){
            toX = mouse.x+cam.x-WIDTH/2;
            toY = mouse.y+cam.y-HEIGHT/2;
            isMoving = true;
        }
    }
}
var newChar = function(charId){
    var l = 500;
    var x = randRange(l,room.w-l);
    var y = randRange(l,room.h-l);
    var char = newEntity(x,y,sprPlayers);
    char.lim = l;
    char.dir = 0;
    var allChars = [
        {x:0,y:0},
        {x:1,y:0},
        {x:2,y:0},
        {x:3,y:0},
        {x:4,y:0},
        {x:0,y:1},
        {x:1,y:1},
        {x:2,y:1},
        {x:3,y:1},
        {x:4,y:1}
    ];
    char.charIndex = allChars[charId];
    var dial1 = '';
    var dial2 = '';
    var dial3 = '';
    var dial4 = '';
    var sprUrl = '';
    var sprUrl2 = '';
    switch(charId){
        case 0:
            dial1 = "Den :";
            dial2 = "Jangan galau yos...";
            dial3 = "Sehat selalu panjang umur lulus semua matkul...";
            dial4 = "JBU...";
            sprUrl = 'https://i.postimg.cc/W33fmsb2/FACE-DEN.png';
            break;
        case 1:
            dial1 = "Eko :";
            dial2 = "Happy Birthday!";
            sprUrl = 'https://i.postimg.cc/9QLkD4Cx/FACE-EKO.png';
            break;
        case 2:
            dial1 = "Er :";
            dial2 = "HBD Yose, wish u all the best yey!";
            dial3 = "Lancar terus ya kuliahnyaaa";
            sprUrl = 'https://i.postimg.cc/N0VPxWym/FACE-ER.png';
            sprUrl2 = 'https://i.postimg.cc/66cfZhcB/50-Funny-Happy-Birthday-Quotes-Wishes-For-Best-Friends-YR-Pa.jpg';
            break;
        case 3:
            dial1 = "Geo :";
            dial2 = "Hbd gal";
            sprUrl = 'https://i.postimg.cc/YCNpZzNv/FACE-GEO.png';
            break;
        case 4:
            dial1 = "Jes :";
            dial2 = "Wish U all the best <3";
            sprUrl = 'https://i.postimg.cc/kXQGPCqM/FACE-JES.png';
            break;
        case 5:
            dial1 = "Misi :";
            dial2 = "Hepi besdei mai bespren 💕🧡💛💚💙💜🖤🌈🌞";
            dial3 = "Aku slamanya gk bakal bebekin km =v=";
            dial4 = "Moga IP++ ᕙ(^▿^-ᕙ)";
            sprUrl = 'https://i.postimg.cc/SNkq8Mbq/FACE-MISI.png';
            sprUrl2 = 'https://i.postimg.cc/kGNJV4K5/misi-pic.jpg';
            break;
        case 6:
            dial1 = "Ton :";
            dial2 = "happy birthday";
            sprUrl = 'https://i.postimg.cc/QtcGKX6H/FACE-TON.png';
            sprUrl2 = 'https://i.postimg.cc/W3BmN9wv/ton-pic.jpg';
            break;
        case 7:
            dial1 = "Wan :";
            dial2 = "Hbd yos .... 🎁";
            sprUrl = 'https://i.postimg.cc/44rPF6xk/FACE-WAN.png';
            break;
        case 8:
            dial1 = "WH :";
            dial2 = "  ///_/// _ ) / _";
            dial3 = " ///_/// _ | // /";
            dial4 = "/// ///___/ /___/";
            sprUrl = 'https://i.postimg.cc/nLRYK7PW/FACE-WH.png';
            break;
    }
    char.dialog = [dial1,dial2,dial3,dial4,sprite(sprUrl,0,0).img,sprite(sprUrl2,0,0).img];
    char.imgIndex = 1;
    char.draw = drawPlayer;
    char.img_xscale = 12;
    char.img_yscale = 12;
    char.toX = x;
    char.toY = y;
    char.spd = 6;
    char.isMoving = false;
    char.timeInt1 = 60;
    char.timeInt2 = 100;
    char.timeInt = randRange(char.timeInt1,char.timeInt2);
    char.move = function(){
        with(char){
            if(isMoving){
                imgIndex += 0.1;
                x += spd*Math.cos(dir*Math.PI/2);
                y -= spd*Math.sin(dir*Math.PI/2);
            }else{
                imgIndex = 1;
                if(time > timeInt){
                    timeInt = randRange(timeInt1,timeInt2);
                    toX = Math.max(lim,Math.min(room.w-lim,x+randRange(-lim,lim)));
                    toY = Math.max(lim,Math.min(room.h-lim,y+randRange(-lim,lim)));
                    isMoving = true;
                }
            }
            if(Math.abs(x-toX) < spd){
                x = toX;
                if(Math.abs(y-toY) < spd){
                    y = toY;
                    if(isMoving){
                        time = 0;
                    }
                    isMoving = false;
                }else{
                    dir = 1+2*(y < toY);
                }
            }else{
                dir = 2*(x > toX);
            }
        }
    }
    return char;
}
for(var i = 0; i < 9; i++){
    chars.push(newChar(i));
    chars[i].img_alpha = 1;
}
var drawTalkButton = function(click){
    c.globalAlpha = 1;
    c.fillStyle = '#F4B9FF';
    c.strokeStyle = '#000';
    c.lineWidth = 9;
    var cx = WIDTH/2;
    var cy = HEIGHT*19/20;
    var dx = 300;
    var dy = 150;
    c.fillRect(cx-dx/2,cy-dy/2,dx,dy);
    c.strokeRect(cx-dx/2,cy-dy/2,dx,dy);
    c.fillStyle = '#000';
    var ftSize = 100;
    c.font = ftSize+"px 'Josefin Sans', sans-serif";
    c.textAlign = 'center';
    c.fillText('TALK',cx,cy+ftSize/2-15);
    if(click && mouse.clicked && mouse.x > cx-dx/2 && mouse.y > cy-dy/2 && mouse.x < cx+dx/2 && mouse.y < cy+dy/2){
        mouse.clicked = false;
        var minDist = 300;
        var which = 0;
        for(var i in chars){
            var curDist = getDist(player.x,player.y,chars[i].x,chars[i].y);
            if(minDist > curDist){
                which = i;
                minDist = curDist;
            }
        }
;
        curDialog = chars[which].dialog;
        isTalking = true;
        letterIndex = 0;
    }
}
var drawDialog = function(d){
    var cx = WIDTH/2;
    var cy = HEIGHT*0.9;
    var w = WIDTH*0.95;
    var h = HEIGHT*1/6;
    var x1 = cx-w/2;
    var y1 = cy-h/2;
    c.globalAlpha = 0.8;
    c.drawImage(d[5],0,HEIGHT/3,d[5].width*2*HEIGHT/3/d[5].height,2*HEIGHT/3);
    c.drawImage(d[4],0,HEIGHT/1.7,d[4].width*HEIGHT/4/d[4].height,HEIGHT/4);
    c.fillStyle = '#000';
    c.fillRect(x1,y1,w,h);
    c.globalAlpha = 1;
    c.fillStyle = '#FFF';
    var ftSize = 50;
    c.font = ftSize+"px 'Josefin Sans', sans-serif";
    c.textAlign = 'left';
    c.fillText(d[0].substring(0,Math.floor(letterIndex++)/3),x1+20,y1+ftSize);
    c.fillText(d[1].substring(0,Math.max(0,Math.floor(letterIndex)/3-d[0].length)),x1+20,y1+ftSize*2);
    c.fillText(d[2].substring(0,Math.max(0,Math.floor(letterIndex)/3-d[0].length-d[1].length)),x1+20,y1+ftSize*3);
    c.fillText(d[3].substring(0,Math.max(0,Math.floor(letterIndex)/3-d[0].length-d[1].length-d[2].length)),x1+20,y1+ftSize*4);
    if(mouse.clicked){
        mouse.clicked = false;
        isTalking = false;
    }
}
setInterval(function(){
    resizeCanvas();
    c.fillStyle = '#FFF';
    c.globalAlpha = 1;
    c.fillRect(0,0,WIDTH,HEIGHT);
    var mapXY = map({x:0,y:0});
    c.drawImage(sprMap.img,mapXY.x,mapXY.y,room.w,room.h);
    if(!canTalk){
        isTalking = false;
    }
    if(canTalk && !isTalking){
        drawTalkButton(true);
    }
    for(var i in Entities){
        Entities[i].update();
    }
    if(canTalk && !isTalking){
        drawTalkButton();
    }
    if(isTalking){
        drawDialog(curDialog);
    }
    TIME++;
    mouse.clicked = false;
},1000/FPS);

});